import * as jwt from "jsonwebtoken";

class Jwt {
  /**
   *
   * @param token bearer auth token from client
   */
  static verifyToken(token: string) {
    return jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
      if (err) {
        const { name, message } = err;

        return { error: true, name, message };
      } else {
        return { error: false, data: decoded };
      }
    });
  }

  static generateToken(): string {
    const token = jwt.sign(
      {
        iss: "Jojonomic",
        iat: 1606696296,
        exp: 1638232296,
        aud: "jojonomic.com",
        sub: "jojoarief",
        company_id: "130",
        user_id: "120",
      },
      process.env.SECRET_KEY
    );

    return token;
  }
}

export default Jwt;
