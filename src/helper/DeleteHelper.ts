const deleteHelper = (cacheData: Array<any>, id) => {
  const index = cacheData.findIndex((x) => x.id === id);
  return cacheData.splice(index, 1);
};

export default deleteHelper;
