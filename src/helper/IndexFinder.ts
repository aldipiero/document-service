const spliceHelper = (cacheData: Array<any>, id, sourceData: Object) => {
  const index = cacheData.findIndex((x) => x.id === id);
  return cacheData.splice(index, 1, sourceData);
};

export default spliceHelper;
