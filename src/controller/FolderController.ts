import Document from "../models/DocumentModel";
import Folder from "../models/FolderModel";
import redis from "../helper/Redis";
import spliceHelper from "../helper/IndexFinder";
import deleteHelper from "../helper/DeleteHelper";

class DocumentController {
  // get data by user company id
  static async getDataByCompanyId(req, res) {
    const company_id = req.company_id;
    const user_id = req.user_id;

    try {
      const dataFromCache = await redis.get(user_id);

      if (dataFromCache) {
        res.json({ error: false, data: JSON.parse(dataFromCache) });
      } else {
        let data: object[] = [];
        // query berdasarkan readme, jujur saya masih kurang begitu jelas dengan perintahnya
        // saya banyak memakan waktu memahami perintahnya, karena cukup membuat rancu
        let documents = await Document.find({ $or: [{ share: { $in: [parseInt(user_id)] } }, { owner_id: parseInt(user_id) }] });
        let folders = await Folder.find({
          $or: [
            { $and: [{ is_public: false }, { $or: [{ share: { $in: [parseInt(user_id)] } }, { owner_id: parseInt(user_id) }] }] },
            { $and: [{ is_public: true }, { company_id }] },
          ],
        });
        data = documents.concat(folders);
        await redis.set(user_id, JSON.stringify(data));
        res.json({ error: false, data });
      }
    } catch (err) {
      res.status(500).json({ error: true, message: err.message });
    }
  }

  // get folder by its id
  static async getPerFolder(req, res) {
    const { folder_id } = req.params;

    try {
      const folderFromCache = await redis.get(folder_id);

      if (folderFromCache) {
        res.json({ error: false, data: JSON.parse(folderFromCache) });
      } else {
        const data = await Document.find({ folder_id });

        await redis.set(folder_id, !data ? null : JSON.stringify(data));

        res.json({ error: false, data });
      }
    } catch (err) {
      res.status(501).json({ error: true, message: err.message });
    }
  }

  // set new folder or update
  static async setFolder(req, res) {
    const { id, name, timestamp, is_public } = req.body;
    const user_id = req.user_id;
    const company_id = req.company_id;

    try {
      const folderById = await Folder.findOne({ id });

      if (folderById) {
        const folder = { id, name, timestamp };
        const data = await Folder.findOneAndUpdate({ id }, folder, { new: true });
        const userCache = await redis.get(user_id);
        const parseCache = JSON.parse(userCache);
        const spliceCache = spliceHelper(parseCache, id, data);
        await redis.set(user_id, JSON.stringify(spliceCache));
        res.json({ error: false, message: "folder updated", data });
      } else {
        const folder = new Folder({ id, name, timestamp, type: "folder", is_public, owner_id: parseInt(user_id), company_id: parseInt(company_id) });
        const data = await folder.save();
        const userCache = await redis.get(user_id);
        let parseUserCache = JSON.parse(userCache);
        parseUserCache.push(data);
        await redis.set(user_id, JSON.stringify(parseUserCache));
        res.json({ error: false, message: "folder created", data });
      }
    } catch (err) {
      res.status(500).json({ error: true, message: err.message });
    }
  }

  // delete folder by its id
  static async deleteFolder(req, res) {
    const { id } = req.body;

    try {
      const data = await Folder.findOneAndDelete({ id });

      if (!data) return res.json({ error: true, message: "Folder not found" });

      const userCache = await redis.get(req.user_id);
      const parseCache = JSON.parse(userCache);
      const deleteCache = deleteHelper(parseCache, id);

      await redis.set(req.user_id, JSON.stringify(deleteCache));
      await redis.set(id, null);

      res.json({ error: false, message: "Success delete folder" });
    } catch (err) {
      res.status(500).json({ error: true, message: err.message });
    }
  }
}

export default DocumentController;
