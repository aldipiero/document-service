import Jwt from "../helper/Jwt";

class LoginController {
  static async login(req, res) {
    const { username, password } = req.body;

    if (username === "admin" && password === "admin") {
      const token = Jwt.generateToken();
      res.json({ error: false, token });
    } else {
      res.status(404).json({ error: true, message: "Invalid login" });
    }
  }
}

export default LoginController;
