import Document from "../models/DocumentModel";
import redis from "../helper/Redis";
import getIndex from "../helper/IndexFinder";
import spliceHelper from "../helper/IndexFinder";
import deleteHelper from "../helper/DeleteHelper";
import { parse } from "dotenv/types";

class DocumentController {
  // set new document or update
  static async setDocument(req, res) {
    const { id, name, type, folder_id, content, timestamp, owner_id, share } = req.body;
    const user_id = req.user_id;
    const company_id = req.company_id;

    try {
      const documentByid = await Document.findOne({ id });

      if (documentByid) {
        const document = { id, name, type, folder_id, content, timestamp, share };
        const data = await Document.findOneAndUpdate({ id }, document, { new: true });
        const userCache = await redis.get(user_id);
        const parseCache = JSON.parse(userCache);
        const spliceCache = spliceHelper(parseCache, id, data);
        await redis.set(user_id, JSON.stringify(spliceCache));
        res.json({ error: false, message: "document updated", data });
      } else {
        const document = new Document({ id, name, type, folder_id, content, timestamp, share, owner_id: owner_id === 0 ? user_id : user_id, company_id: parseInt(company_id) });
        const data = await document.save();
        const userCache = await redis.get(user_id);
        let parseUserCache = JSON.parse(userCache);
        parseUserCache.push(data);
        await redis.set(user_id, JSON.stringify(parseUserCache));
        res.json({ error: false, message: "document created", data });
      }
    } catch (err) {
      res.status(500).json({ error: true, message: err.message });
    }
  }

  static async getDetailDocument(req, res) {
    const { document_id } = req.params;

    try {
      const documentFromCache = await redis.get(document_id);
      if (!documentFromCache) {
        const data = await Document.findOne({ id: document_id });
        if (!data) return res.json({ error: true, message: "Document not found" });
        await redis.set(document_id, JSON.stringify(data));
        res.json({ error: false, data });
      } else {
        res.json({ error: false, data: JSON.parse(documentFromCache) });
      }
    } catch (err) {
      res.status(501).json({ error: true, message: err.message });
    }
  }

  static async deleteDocument(req, res) {
    const { id } = req.body;

    try {
      const data = await Document.findOneAndDelete({ id });

      if (!data) res.json({ error: true, message: "Document not found" });

      const userCache = await redis.get(req.user_id);
      const parseCache = JSON.parse(userCache);
      const deleteCache = deleteHelper(parseCache, id);

      await redis.set(req.user_id, JSON.stringify(deleteCache));
      await redis.del(id);

      res.json({ error: false, message: "Success delete document" });
    } catch (err) {
      res.status(500).json({ error: true, message: err.message });
    }
  }
}

export default DocumentController;
