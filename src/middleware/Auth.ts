import Jwt from "../helper/Jwt";

class Auth {
  static tokenCheck(req, res, next) {
    const authHeader = req.headers.authorization;

    if (authHeader) {
      const token = authHeader.split(" ")[1];

      const { error, name, message, data } = Jwt.verifyToken(token);

      if (!error) {
        const { user_id, company_id } = data;
        req.user_id = user_id;
        req.company_id = company_id;
        next();
      } else {
        res.status(401).json({ error, message });
      }
    } else {
      res.json({ error: true, message: "Missing authentication token." });
    }
  }
}

export default Auth;
