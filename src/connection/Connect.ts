import * as mongoose from "mongoose";

class Connect {
  static async Db() {
    try {
      const mo = await mongoose.connect(process.env.MONGO_URI, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
      });

      console.log(`Connected to server: ${mo.connection.host}`);
    } catch (err) {
      console.error(`${err.message}`);
    }
  }
}

export default Connect;
