import * as express from "express";
import * as cors from "cors";
import * as morgan from "morgan";
import Connect from "./connection/Connect";
import router from "./router";

const PORT = process.env.PORT || 3000;

require("dotenv").config();
Connect.Db();
const app = express();

app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/document-service", router);

app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
});
