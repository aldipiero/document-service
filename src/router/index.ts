import { Router } from "express";
import Auth from "../middleware/Auth";
import FolderController from "../controller/FolderController";
import DocumentController from "../controller/DocumentController";
import LoginController from "../controller/LoginController";

const route = Router();

// token validation on every request
route.use(Auth.tokenCheck);

// generate token
route.post("/login", LoginController.login);

// folder route controller
route.get("/", FolderController.getDataByCompanyId);
route.get("/folder/:folder_id", FolderController.getPerFolder);
route.post("/folder", FolderController.setFolder);
route.delete("/folder", FolderController.deleteFolder);

// document contoller route
route.get("/document/:document_id", DocumentController.getDetailDocument);
route.post("/document", DocumentController.setDocument);
route.delete("/document", DocumentController.deleteDocument);

route.get("*", function (req, res) {
  res.status(404).json({ error: false, message: "Url not found" });
});

export default route;
