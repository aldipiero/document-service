import * as mongoose from "mongoose";

const folderSchema = new mongoose.Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  type: { type: String, required: true },
  timestamp: { type: Number, required: true },
  is_public: { type: Boolean, required: true },
  content: { type: Object },
  owner_id: { type: Number, required: true },
  company_id: { type: Number, required: true },
});

const folderModel = mongoose.model("Folder", folderSchema);

export default folderModel;
