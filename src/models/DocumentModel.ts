import * as mongoose from "mongoose";

const documentSchema = new mongoose.Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  type: { type: String, required: true },
  folder_id: { type: String },
  content: { type: Object, required: true },
  timestamp: { type: Number, required: true },
  owner_id: { type: Number, required: true },
  share: { type: Array },
  company_id: { type: Number },
});

const documentModel = mongoose.model("Document", documentSchema);

export default documentModel;
